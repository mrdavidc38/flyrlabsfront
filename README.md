# Hello!
Hola, agradecimientos.
# FlyrlabsFront

Este proyecto fue creado con la versión de angular ClI 16, tambien con Node 16 y ademas algunas librerías utilizadas para su funcionamiento como :

- npm i ngx-spinner
- npm i ng2-currency-mask
- ng add @angular/material



El proyecto fue creado especificamente para el consumo de el enpoint: https://recruiting-api.newshore.es/api/flights/1,  para los demás el resultado puede ser erroneo, especialmente para https://recruiting-api.newshore.es/api/flights/2, aunque la logica es posible que pueda adaptarse para un numero N de rutas.

Se contruyó un API en .Net 6 tipo "fachada" que consume el servicio antes mencionado como una integración (arquitectura y caracteristicas estarán en su correspondiente README).

La app web esá hecha con el patrón de "arquitectura limpia" con sus correspondientes capa compartida, de acceso a datos y de logica. La aplicación valida una ruta unica o directa (si existe), y otras dos con escalas (si existen), cada ruta contiene un total con un botón que permite cambiar el valor de cada ruta en Euros ó Pesos colombianos. Se seteó la ruta que según el excel contiene una ruta única y dos con escalas, esto en el primer acceso a la ruta. El proyecto en GitLab contiene dos rams pricipales una Main a la cual le ahgo merge con Develops.

Quise impplementar un Login sencillo y tokenizar con JWT el API, ademas de usar NGX store para controlar el estado 
de la app, en este caso del objeto solícitado con las rutas finales, pero por temas de tiempo me fue complicado implementarlos, tambien tenia dudas en las posibles combinaciones que podian tomar los destinos y los origenes asi que la logica de momento es muy especifica, la tabla que contruí para las rutas puede ser mas dínamica ya que en caso de rutas únicas no es necesario las demás, eso me faltó por mejorar tambien.

https://gitlab.com/mrdavidc38/flyrlabsfront/-/blob/main/home.png?ref_type=heads
https://gitlab.com/mrdavidc38/flyrlabsfront/-/blob/main/requesFly.png?ref_type=heads

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
