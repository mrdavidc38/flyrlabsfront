import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AvailableRoutesRoutingModule } from './infraestructure/available-routes-routing.module';
import { FlyrequestComponent } from './ui/flyrequest/flyrequest.component';
import { FlyavailableComponent } from './ui/flyavailable/flyavailable.component';
import { AvailableRoutesGateway } from './domain/gateway/available-routes-gateway';
import { AvailableroyesUseCase } from './domain/usecase/availableroyes-use-case';
import { AvailableRoutesApiService } from './infraestructure/driven-adapter/available-routes-api.service';
import { SharedModule } from '../shared/shared.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CurrencyMaskModule } from 'ng2-currency-mask';


@NgModule({
  declarations: [
    FlyrequestComponent,
    FlyavailableComponent
  ],
  imports: [
    CommonModule,
    AvailableRoutesRoutingModule,
    SharedModule,
    NgxSpinnerModule
    ,CurrencyMaskModule

  ],
  exports : [
    FlyrequestComponent,
    FlyavailableComponent
  ],
  providers: [
    AvailableroyesUseCase,
    {provide: AvailableRoutesGateway, useClass: AvailableRoutesApiService}
],
bootstrap: [FlyrequestComponent],
 
})
export class AvailableRoutesModule { }
