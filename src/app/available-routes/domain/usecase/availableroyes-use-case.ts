import { Injectable } from "@angular/core"
import { AvailableRoutesGateway } from "../gateway/available-routes-gateway"

@Injectable()
export class AvailableroyesUseCase {
    constructor(private _availableroutes: AvailableRoutesGateway) { }
    getRoutesFly(path:string)
    {
        return this._availableroutes.getRoutesFly(path);
    }
 
}