import { Observable } from "rxjs";
import { getFlyRoutesOut } from "src/app/shared/domain/MethodParameters/getFlyRoutesOut";

export abstract class AvailableRoutesGateway {
    abstract getRoutesFly(path?: string): Observable<getFlyRoutesOut>;

}