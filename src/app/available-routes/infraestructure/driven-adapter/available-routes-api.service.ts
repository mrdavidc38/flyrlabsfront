import { Injectable } from '@angular/core';
import { AvailableRoutesGateway } from '../../domain/gateway/available-routes-gateway';
import { Observable, map } from 'rxjs';
import { getFlyRoutesOut } from 'src/app/shared/domain/MethodParameters/getFlyRoutesOut';
import { HttpClient } from '@angular/common/http';
import { BaseApiService } from 'src/app/shared/base-api.service';

@Injectable()
export class AvailableRoutesApiService extends AvailableRoutesGateway{

  constructor(private http: BaseApiService,
    private https: HttpClient) { super();}


   getRoutesFly(path: string ): Observable<getFlyRoutesOut> {
    return this.http.get(path).pipe(map((Response => Response as getFlyRoutesOut)));
  }

  
}
