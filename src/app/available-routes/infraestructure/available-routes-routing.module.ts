import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlyrequestComponent } from '../ui/flyrequest/flyrequest.component';
import { FlyavailableComponent } from '../ui/flyavailable/flyavailable.component';
import { permissions } from 'src/app/shared/permissions/permissions.guard';

const routes: Routes = [
{
    path : 'flyrequest',
    canActivate:[permissions],
    component : FlyrequestComponent,
    loadChildren : () => import ('src/app/available-routes/available-routes.module').then(module => module.AvailableRoutesModule)
},
{
  path : 'flyravailable',
  component : FlyavailableComponent,
  loadChildren : () => import ('src/app/available-routes/available-routes.module').then(module => module.AvailableRoutesModule)
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AvailableRoutesRoutingModule { }
