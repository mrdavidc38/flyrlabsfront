import { Component, OnInit, ViewChild } from '@angular/core';
import { AvailableRoutesApiService } from '../../infraestructure/driven-adapter/available-routes-api.service';
import { AvailableroyesUseCase } from '../../domain/usecase/availableroyes-use-case';
import { Result } from 'src/app/shared/domain/Entities/Result';
import { getFlyRoutesOut } from 'src/app/shared/domain/MethodParameters/getFlyRoutesOut';
import { RoutesResponse } from 'src/app/shared/domain/Entities/RouteResponse';
import { Journey } from 'src/app/shared/domain/Entities/Journey';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/shared/customValidators/customValidators';
import { Flight } from 'src/app/shared/domain/Entities/Flight';
import { MatTableDataSource } from '@angular/material/table';
import { Transport } from 'src/app/shared/domain/Entities/Transport';
import { dataSource } from 'src/app/shared/domain/Entities/dataSource';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-flyrequest',
  templateUrl: './flyrequest.component.html',
  styleUrls: ['./flyrequest.component.css']
})
export class FlyrequestComponent implements OnInit {
  columnasTabla : string[] = ['Origin','Destination','Price','FlightCarrier','FlightNumber', 'acciones'];
  dataInicio = [{Origin:'Origin',Destination:'Destination',Price:'Price',FlightCarrier:'FlightCarrier',FlightNumber:'FlightNumber'}];
  dataSourceLis : dataSource[] = [];
  dataSour :dataSource[] = [{Origin:'Origin',Destination:'Destination',Price:0,FlightCarrier:'FlightCarrier',FlightNumber:'FlightNumber'}];
  dataListaUsuarios = new MatTableDataSource(this.dataSour);
  originIni :string = '';
  destinationIni : string = '';
  priceIni : string = '';
  routesFlys : getFlyRoutesOut ={
    listFlys: []
  } ;
  match = false;
  Journey : Journey ={
    flights: [],
    origin: '',
    destination: '',
    price: 0
  };
  flysForm !: FormGroup;
  @ViewChild('timeOutModal') timeOutModal!: any;
  monstrarLoadinfg = false;
  constructor(private useCaseFly :AvailableroyesUseCase,
              private router: Router, 
              private fb : FormBuilder,
              private spinner: NgxSpinnerService,)
{}
  ngOnInit(): void {
    // this.routesFlys.listFlys = [];
 
    debugger;
    localStorage.clear();

    this.flysForm = this.createFormFlys();
    this.getRoutesFly();
  }

  getRoutesFly()
  {
    this.spinner.show();
    setTimeout(() => {
      this.useCaseFly.getRoutesFly('routesflys/listaRutas').subscribe({
     
        next: resp =>{ if(resp.result == Result.Success)
                    {
                      this.routesFlys.listFlys =[];
                        this.routesFlys.listFlys = resp.listFlys
                        this.spinner.hide();
                        
                    }},
                    error: (err) =>
                    {this.monstrarLoadinfg = false;
                      console.error('error',err);
                      this.spinner.hide();
                    },
    
    complete: () =>
    {this.spinner.hide();
      console.log('correcto');
      
    }
     
    });
    }, 2000);

  }

  availableRoutes(departura : string, arrival : string)
  {
    
    this.dataSourceLis = [];
    var newFkysList :any[];
    this.Journey.flights = [];
    this.Journey.destination = arrival;
    this.Journey.origin = departura;
    newFkysList = this.routesFlys.listFlys;
    var routes = newFkysList.filter(fly => fly.departureStation == departura )
    
    //-------------------------------ruta directa------------------------------------
    var uniqueRoutes = routes.filter(unique => unique.arrivalStation == arrival);

    var transportRoute : Transport ={
      flightCarrier: '',
      flightNumber: ''
    }
    var flyRoute : Flight = {
    transport: transportRoute,
    origin: 'Ruta unica',
    destination: '',
    price: 0
  };

  this.Journey.flights.push(flyRoute);

  uniqueRoutes.forEach(rout =>{
    var transport1 : Transport ={
      flightCarrier: '',
      flightNumber: ''
    }
    var fly1 : Flight = {
    transport: transport1,
    origin: '',
    destination: '',
    price: 0
  };
  

  fly1.origin = rout.departureStation;
  fly1.destination = rout.arrivalStation;
  fly1.price = rout.price;
  fly1.transport.flightCarrier = rout.flightCarrier;
  fly1.transport.flightNumber = rout.flightNumber;
  this.Journey.flights.push(fly1);
  });

  var total = 0;
  uniqueRoutes.forEach(rou =>{
    if(rou.departureStation != 'Ruta unica' && rou.departureStation !=  'Total' )
    {
        total += rou.price;
    }

  })

  var transport2 : Transport ={
    flightCarrier: '',
    flightNumber: ''
  }
  var fly2 : Flight = {
  transport: transport2,
  origin: '',
  destination: '',
  price: 0
};

fly2.origin = 'Total';
fly2.destination = '';
fly2.price = total;
fly2.transport.flightCarrier = '';
fly2.transport.flightNumber = '';
this.Journey.flights.push(fly2);
    //--------------------------------------------------------------------------------

    //----------------quito las rutas iniciales del arreglo principal-----------------
    
    routes.forEach(element => {
      
      newFkysList= newFkysList.filter(fly => fly.flightNumber != element.flightNumber);
      newFkysList;
    });
    //--------------------------------------------------------------------------------

    //----------------segundo nivel: evaluo rutas donde el destino de las -----------------
    //----------------primeras coincida con el origen en el arreglo principal-----------------
   var routes2 : any[] = new Array<any>(); 
   var routes3 : any[] = new Array<any>(); 
    var price = 0;
    newFkysList.forEach(elemnt =>{ 
      routes.forEach(rout =>{
        if(rout.arrivalStation == elemnt.departureStation)
        {
          routes2.push(elemnt)
          if(elemnt.arrivalStation == arrival)
          {
            price2 += elemnt.price;

            routes3.push(rout,elemnt);
          }
        }
      })
    })

    var uniqueRoutes2 = new Set(routes2);
    const uniqueRoutes2Array = [...uniqueRoutes2]

    var transportRoutes : Transport ={
      flightCarrier: '',
      flightNumber: ''
    }
    var flyRoutes : Flight = {
    transport: transportRoutes,
    origin: 'Ruta 1',
    destination: '',
    price: 0
  };

  this.Journey.flights.push(flyRoutes);
  routes3.forEach(rout =>{
      var transport : Transport ={
        flightCarrier: '',
        flightNumber: ''
      }
      var fly : Flight = {
      transport: transport,
      origin: '',
      destination: '',
      price: 0
    };
    

    fly.origin = rout.departureStation;
    fly.destination = rout.arrivalStation;
    fly.price = rout.price;
    fly.transport.flightCarrier = rout.flightCarrier;
    fly.transport.flightNumber = rout.flightNumber;
    this.Journey.flights.push(fly);
    });

    var total3 = 0;
    routes3.forEach(rout =>{
      if(rout.departureStation != 'Ruta 1' && rout.departureStation !=  'Total')
      {
        total3 += rout.price;
      }
 
    })

    var transport : Transport ={
      flightCarrier: '',
      flightNumber: ''
    }
    var fly : Flight = {
    transport: transport,
    origin: '',
    destination: '',
    price: 0
  };

  fly.origin = 'Total';
  fly.destination = '';
  fly.price = total3;
  fly.transport.flightCarrier = '';
  fly.transport.flightNumber = '';
  this.Journey.flights.push(fly);
  //--------------------------------------------------------------------------------

  //----------------quito las rutas routes2 iniciales del arreglo principal---------
       routes2.forEach(element => {
         
         newFkysList= newFkysList.filter(fly => fly.flightNumber != element.flightNumber);
         newFkysList;
       });
  //--------------------------------------------------------------------------------

  //----------------Tercer nivel: evaluo rutas donde el destino de las -----------------
  //----------------primeras coincida con el origen en el arreglo principal-----------------
   var routes4 : any[] = new Array<any>(); 
   var routes5 : any[] = new Array<any>(); 
   var count = 0;
   var price2 = 0;

    newFkysList.forEach(elemento =>{ 
      count ++;
      routes2.forEach(rout =>{   
      if( rout.arrivalStation == elemento.departureStation)
        {
          routes4.push(elemento);
          if(elemento.arrivalStation == arrival)
          {
            price2 += elemento.price;
            routes5.push(rout,elemento);
          }
        } 
      })
    
    })

    routes4.forEach(element => {
         
      newFkysList= newFkysList.filter(fly => fly.flightNumber != element.flightNumber);
      newFkysList;
    });

    if(routes5.length > 0)
    {
      var firtsRouyte = routes.filter(r =>r.arrivalStation == routes5[0].departureStation);
      var firtsRouyteOb :RoutesResponse ={
        departureStation: firtsRouyte[0].departureStation,
        arrivalStation: firtsRouyte[0].arrivalStation,
        flightCarrier: firtsRouyte[0].flightCarrier,
        flightNumber: firtsRouyte[0].flightNumber,
        price: firtsRouyte[0].price
      }
      routes5.unshift(firtsRouyteOb);
    }
   
    
    this.Journey.price = price;
    // this.Journey.flights = routes5;
    var uniqueRoutes4 = new Set(routes4);
    const uniqueRoutes4Array = [...uniqueRoutes4]

    var transportRoute2 : Transport ={
      flightCarrier: '',
      flightNumber: ''
    }
    var flyRoute2 : Flight = {
    transport: transportRoute2,
    origin: 'Ruta 2',
    destination: '',
    price: 0
  };

  this.Journey.flights.push(flyRoute2);
    routes5.forEach(rout =>{
      var transport : Transport ={
        flightCarrier: '',
        flightNumber: ''
      }
      var fly : Flight = {
      transport: transport,
      origin: '',
      destination: '',
      price: 0
    };
    

    fly.origin = rout.departureStation;
    fly.destination = rout.arrivalStation;
    fly.price = rout.price;
    fly.transport.flightCarrier = rout.flightCarrier;
    fly.transport.flightNumber = rout.flightNumber;
    this.Journey.flights.push(fly);
    })
    var total = 0;

    routes5.forEach(rout =>{
      if(rout.departureStation != 'Ruta 2' && rout.departureStation !=  'Total')
      {
          total += rout.price;
      }
 
    })

    var transport3 : Transport ={
      flightCarrier: '',
      flightNumber: ''
    }
    var fly3 : Flight = {
    transport: transport3,
    origin: '',
    destination: '',
    price: 0
  };

  fly3.origin = 'Total';
  fly3.destination = '';
  fly3.price = total;
  fly3.transport.flightCarrier = '';
  fly3.transport.flightNumber = '';
  this.Journey.flights.push(fly3);
  //--------------------------------------------------------------------------------

  //  this.conversionToDataSource();
   if(uniqueRoutes.length == 0 && routes3.length == 0 && routes5.length ==0)
   {
    alert('No existe una ruta  para tu viaje');
    this.dataSour =[];
    this.dataSour = [{Origin:'Origin',Destination:'Destination',Price:0,FlightCarrier:'FlightCarrier',FlightNumber:'FlightNumber'}];;

   }
   else if( routes3.length == 0 && routes5.length ==0 )
   {
    alert('Solo existe una ruta dirtecta para tu viaje');
    routes3 =[];
    routes5 = [];
    this.conversionToDataSource();
   }
   else{
    this.conversionToDataSource();
   }  
   this.flysForm.get('principalOrigin')?.setValue(this.originIni);
   this.flysForm.get('principalDestinaiton')?.setValue(this.destinationIni);
   this.flysForm.get('pricipalPrice')?.setValue(this.priceIni);


 //--------------------------------------------------------------------------------

  }

  createFormFlys()
  {
    let form = this.fb.group({
      origin: ['MZL', [Validators.required, CustomValidators.MaxLength(3)]],
      destination: ['CTG', [Validators.required,CustomValidators.MaxLength(3),]],
      
      principalOrigin: [ { value: undefined, disabled: true }],
      principalDestinaiton: [{ value: undefined, disabled: true },],
      pricipalPrice: [{ value: undefined, disabled: true } ],
      currencyConversion: [],

    })

    form.get('currencyConversion')?.valueChanges.subscribe( value =>{
      debugger;
      var euro = 0;
      var pesos = 0;
        if(value == '1')
        {
          var intValue = this.flysForm.get('pricipalPrice')?.value;
          ;
          euro = intValue*0.93;
          this.flysForm.get('pricipalPrice')?.setValue(euro);
        }
        else{
          var intValue = this.flysForm.get('pricipalPrice')?.value;
          ;
          pesos = intValue*3909;
          this.flysForm.get('pricipalPrice')?.setValue(pesos);
        }
    });

    
    form.get('origin')?.valueChanges.subscribe( value =>{
      if(value != null)
      {
        if(value.trim().length == 0)
        {
          form.get('origin')?.reset();
        }
    }});


    
    form.get('destination')?.valueChanges.subscribe( value =>{
      if(value != null)
      {
        if(value.trim().length == 0)
        {form.get('destination')?.reset();
      }
    }});
    return form;
  }


  conversionToDataSource()
{
  this.dataSourceLis = [];
  this.Journey.flights.forEach(fly =>{
    var dataS : dataSource = {
      Origin: '',
      Destination: '',
      Price: 0,
      FlightCarrier: '',
      FlightNumber: '',
      
    }
    dataS.Origin = fly.origin;
    dataS.Destination = fly.destination;
    dataS.Price = fly.price;
    dataS.FlightCarrier = fly.transport.flightCarrier;
    dataS.FlightNumber = fly.transport.flightNumber;
    this.dataSourceLis.push(dataS);
  });
  this.dataSour = [];
  this.dataSour = this.dataSourceLis;

}
 
routesCalculation()
{
  debugger;
  var origin = '';
  var destination = '';
  if(!this.flysForm.invalid)
  {
  origin = this.flysForm.get('origin')?.value;
  destination = this.flysForm.get('destination')?.value;
  this.originIni = origin;
  this.destinationIni = destination;
  this.availableRoutes(origin, destination);

  }
 
}

validateMatch()
{
  debugger;
  var origin = '';
  var destination ='';
   origin = this.flysForm.get('origin')?.value;
   destination = this.flysForm.get('destination')?.value;

  if(origin === destination)
  {
    this.match = true;
  }
  else{
    this.match = false
  }
}
setPrincipalPrice(input : any)
{
  this.flysForm.get('pricipalPrice')?.setValue(input.Price);
}
comeBack()
{
  localStorage.clear();
  this.router.navigate(['Home'])
}

ngOnDestroy(): void {

  setTimeout(() => {

  }, 1000);
  this.router.navigate(['Home'])

}
}
