import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AvailableRoutesGateway } from './available-routes/domain/gateway/available-routes-gateway';
import { AvailableroyesUseCase } from './available-routes/domain/usecase/availableroyes-use-case';
import { AvailableRoutesApiService } from './available-routes/infraestructure/driven-adapter/available-routes-api.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
  
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, 
    AppRoutingModule,
    HttpClientModule
  ],
  
  providers: [
    AvailableroyesUseCase,
    {provide: AvailableRoutesGateway, useClass: AvailableRoutesApiService}
],
  bootstrap: [AppComponent]
})
export class AppModule { }
