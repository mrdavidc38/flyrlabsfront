import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlyavailableComponent } from './available-routes/ui/flyavailable/flyavailable.component';
import { HomeComponent } from './request-fly/ui/home/home.component';

export const routes: Routes = [
  {
    path: '',
 
    loadChildren: () => import('./request-fly/request-fly.module').then(module => module.RequestFlyModule),
    component : HomeComponent

  },
  {
    path: '',
    loadChildren: () => import('./available-routes/available-routes.module').then(module => module.AvailableRoutesModule),
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
