import { ActivatedRouteSnapshot, CanActivate, CanActivateFn, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class permissions implements CanActivate  {
  constructor(private router:Router)
  {}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    var val = localStorage.getItem('paso');
   if(val === '1')
   {
    return true;
   }
  else{
    this.router.navigate(["Home"])
    return false;}
  }
  
  
};
