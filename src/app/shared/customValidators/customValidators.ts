import { AbstractControl, ValidatorFn, ValidationErrors, Validators,  } from "@angular/forms";

export class CustomValidators {



  static MaxLength(maxLength: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const length: number = control.value ? control.value.length : 0;
      if(length > maxLength) {
        control.setValue(control.value.slice(0,maxLength));
      }
      return null;
    };
  }


}
