export interface RoutesResponseHelper {
    departureStation: string;
    arrivalStation: string;
    flightCarrier: string;
    flightNumber: string;
    price: number;
    flyNumberID : number
}