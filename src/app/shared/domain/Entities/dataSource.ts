export interface dataSource {
    Origin : string;
    Destination : string;
    Price : number;
    FlightCarrier : string;
    FlightNumber : string;
   
}