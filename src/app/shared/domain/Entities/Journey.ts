import { Flight } from "./Flight";

export interface Journey {
    flights: Flight[];
    origin: string;
    destination: string;
    price: number;
}