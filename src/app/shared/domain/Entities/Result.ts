export enum Result {
    Success,
    Error,
    RecordAlreadyExists = 3,
    InactiveUser = 6,
    ErrorUser = 4,
    NoRecords = 2
  
  }