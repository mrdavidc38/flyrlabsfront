import { Result } from "../Entities/Result";

export interface BaseResponse {
  result?: Result;
  message?: string;
}
