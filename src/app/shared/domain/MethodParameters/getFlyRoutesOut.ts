import { RoutesResponse } from "../Entities/RouteResponse";
import { BaseResponse } from "./base-response";

export interface getFlyRoutesOut extends BaseResponse{
    listFlys: RoutesResponse[];
}