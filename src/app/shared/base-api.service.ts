import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class BaseApiService {
  private url = environment.apiUrl;

  constructor(private readonly http: HttpClient) { }

  get(path: string, headers?:HttpHeaders) {
    return this.http.get(`${this.url}${path}`);
  }

  // post(path: string, data: any, headers?:HttpHeaders) {
 
  //   return this.http.post(`${this.url}${path}`, this.encryptUsingAES256(JSON.stringify(data)).toString() , { headers : headers?.append('x-functions-key',environment.apiUrlParams) } );
  // }

  // put(path: string, data: any) {
  //   return this.http.put(`${this.url}${path}${this.apiUrlParams}`, data);
  // }

  // putWithHeaders(path: string, data: any, headers?: HttpHeaders) {
  //   return this.http.put(`${this.url}${path}`, this.encryptUsingAES256(JSON.stringify(data)).toString() , { headers : headers?.append('x-functions-key',environment.apiUrlParams) });
  // }

  // delete(path: string,) {
  //   return this.http.delete(`${this.url}${path}${this.apiUrlParams}`);
  // }
  // encryptUsingAES256(data: string) {
  //   var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(JSON.stringify(data)), this.key, {
  //     keySize: 128 / 8,
  //     iv: this.iv,
  //     mode: CryptoJS.mode.CBC,
  //     padding: CryptoJS.pad.Pkcs7
  //   });

  //   return encrypted;
  // }
}
