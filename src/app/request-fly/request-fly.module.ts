import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { RequestFlyRoutingModule } from './infraestructure/request-fly-routing.module';
import { HomeComponent } from './ui/home/home.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    RequestFlyRoutingModule,
   
    SharedModule
  ]
  ,exports: [HomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RequestFlyModule { }
