import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../ui/home/home.component';


const routes: Routes = [
  {
    path : 'Home',
    component : HomeComponent,
    loadChildren : () => import('src/app/request-fly/request-fly.module').then(module => module.RequestFlyModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestFlyRoutingModule { }
