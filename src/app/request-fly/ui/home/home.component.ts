import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  ngOnInit(): void {
    localStorage.clear();
  }

  constructor(private router : Router)
  {

  }

  ingreso()
  {
    localStorage.setItem('paso', '1');
    this.router.navigate(["flyrequest"]);
  }
}
