import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlysearchComponent } from './flysearch.component';

describe('FlysearchComponent', () => {
  let component: FlysearchComponent;
  let fixture: ComponentFixture<FlysearchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FlysearchComponent]
    });
    fixture = TestBed.createComponent(FlysearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
